normalize_names <- function(x) {
  punctuation_removed <- stringr::str_replace_all(x, "[[:punct:]]|[[:space:]]", "_")
  stringr::str_to_lower(punctuation_removed)
}

normalize_matrix <- function(mat, normalize_diag = TRUE) {
  cleaned_row_names <- normalize_names(rownames(mat))
  cleaned_col_names <- normalize_names(colnames(mat))
  stopifnot(identical(cleaned_row_names, cleaned_col_names))

  dimnames(mat) <- list(cleaned_row_names, cleaned_col_names)
  sorted_names <- sort(cleaned_row_names)
  mat <- mat[sorted_names, sorted_names]

  if (isTRUE(normalize_diag)) {
    diag(mat) <- TRUE
  }

  mat
}

read_raw_matrix <-function(path, ...) {
  mat <- as.matrix(read.csv(path, row.names = 1L))
  normalize_matrix(mat, ...)
}

read_raw_data <- function(dataset, dataname, ...) {
  read_raw_matrix(file.path("data-raw", dataset, paste0(dataname, ".csv")), ...)
}

read_raw_reference <- function(dataset) {
  ref <- transitive_closure(read_raw_data(dataset, "reference"))
  diag(ref) <- 1
  ref | FALSE
}

read_raw_patterns <- function(dataset) {
  files <- list.files(file.path("data-raw", dataset), pattern = "^pattern.*\\.csv$")
  ms <- lapply(tools::file_path_sans_ext(files), read_raw_data, dataset = dataset)
  pat <- Reduce("|", ms)
  diag(pat) <- TRUE
  pat
}

read_raw <- function(dataset, embeddings_model) {
  reference <- read_raw_reference(dataset)
  words <- colnames(reference)

  hits <- read_raw_data(dataset, "hits", normalize_diag = FALSE)
  patterns <- read_raw_patterns(dataset)

  embeddings <- find_embeddings(embeddings_model, words)

  list(
    reference = reference,
    words = words,
    hits = hits,
    patterns = patterns,
    embeddings = embeddings
  )
}