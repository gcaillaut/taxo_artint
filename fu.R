offset_vectors_hypernyms <- function(embeddings, ref, na.rm = FALSE) {
  diag(ref) <- FALSE
  word_pairs <- which(ref, arr.ind = TRUE, useNames = FALSE)
  offsets <- unname(embeddings[word_pairs[, 1L], , drop = FALSE] - embeddings[word_pairs[, 2L], , drop = FALSE])

  if (isTRUE(na.rm)) {
    nas <- apply(offsets, 1, anyNA)
    word_pairs <- word_pairs[!nas, , drop = FALSE]
    offsets <- offsets[!nas, , drop = FALSE]
  }
  list(pairs = unname(word_pairs), offsets = unname(offsets))
}

fu_projections <- function(embeddings, ref, k = 50L, kmeanscount = 50L) {
  # ref <- dag_transitive_reduction(ref, ref)
  hypernym_offsets <- offset_vectors_hypernyms(embeddings, ref, na.rm = TRUE)
  offsets <- hypernym_offsets$offsets
  pairs <- hypernym_offsets$pairs

  # clustering <- kmeans(offsets, centers = k)
  maxk <- min(nrow(na.omit(offsets)), nrow(offsets) - 1L)
  k <- min(maxk, k)

  if (k < 1L) {
    cat("Cannot do Fu\n")
    return(list(centers = NA, projections = NA))
  }

  # clusterings <- lapply(seq_len(kmeanscount), function(i) kmeans(offsets, centers = k))
  # bss <- vapply(clusterings, function(cl) cl$betweenss, numeric(1L))
  # tss <- vapply(clusterings, function(cl) cl$totss, numeric(1L))
  # ratio_bss_tss <- bss / tss
  # clustering <- clusterings[[which.max(ratio_bss_tss)]]
  clustering <- kmeans(offsets, centers = k)

  projections <- lapply(seq_len(k), function(i) {
    subset <- which(clustering$cluster == i)
    hypernyms_index <- pairs[subset, 1L]
    hyponyms_index <- pairs[subset, 2L]

    hyponyms <- embeddings[hyponyms_index, , drop = FALSE]
    hypernyms <- embeddings[hypernyms_index, , drop = FALSE]

    mod <- lm(hypernyms ~ hyponyms + 0)
    mod
  })

  list(centers = clustering$centers, projections = projections)
}

euclidean_distance <- function(x, y) {
  sqrt(sum((y - x) ^ 2))
}

fu_neighborhood_from_projection <- function(centers, projections, embeddings, threshold) {
  if (is.na(centers) || is.na(projections)) {
    return(diag(nrow(embeddings)) | FALSE)
  }

  n <- nrow(embeddings)
  offsets <- array(0, dim = c(n, n, ncol(embeddings)))
  dist_to_centers <- array(0, dim = c(n, n, nrow(centers)))

  for (i in seq_len(n)) {
    for (j in seq_len(n)) {
      offsets[i, j, ] <- embeddings[j, ] - embeddings[i, ]
      dist_to_centers[i, j, ] <- apply(centers, 1L, euclidean_distance, offsets[i, j, ])
    }
  }

  words <- rownames(embeddings)
  res <- matrix(FALSE, nrow = n, ncol = n, dimnames = list(words, words))
  for (i in seq_len(n)) {
    if (anyNA(embeddings[i, ])) {
      next
    }

    for (j in seq_len(n)) {
      if (anyNA(embeddings[j, ])) {
        next
      }

      best_center <- which.min(dist_to_centers[i, j, ])
      mod <- projections[[best_center]]
      predicted <- predict(mod, newdata = list(hyponyms = embeddings[i, , drop = FALSE]))
      d <- euclidean_distance(predicted, embeddings[j, ])
      if (d <= threshold) res[i, j] <- TRUE
    }
  }

  diag(res) <- TRUE
  res
}

# # k => nombre de cluster
# fu_neighborhood <- function(reference, embeddings, threshold, k) {
#   projections <- fu_projections(embeddings, reference, k = k)
#   fu_neighborhood_from_projection(projections$centers, projections$projections, embeddings, threshold)
# }

fu_predicate <- function(centers, projections, embeddings, threshold) {
  fu_neighborhood <- fu_neighborhood_from_projection(centers, projections, embeddings, threshold)
  inner <- neighborhoods_predicate(fu_neighborhood)
  structure(
    list(centers = centers, models = projections, embeddings = embeddings, inner_predicate = inner),
    class = c("fu_predicate", "predicate")
  )
}

predicate_values.fu_predicate <- function(p, set, x) {
  predicate_values(p$inner_predicate, set, x)
}
